 const pool = require('../db');

const login = async (req, res, next) => {
    try {
        const { nombre_usuario, contrasena_usuario} = req.body;
        
        const allUsuario = await pool.query("SELECT * FROM docente where contrasena_usuario = $1 and nombre_usuario = $2" ,
        [contrasena_usuario,
        nombre_usuario]
        );
        // res.json(allUsuario.rows)
        if(allUsuario.rowCount == 0){
            res.json("usuario no existe")
        }else if(allUsuario.rowCount >= 1){
            res.json("usuario existe")
        }
        
        

    } catch (error) {
        next(error);
    }

}
const getAllUsuario = async (req, res, next) => {
    try {
        const allUsuario = await pool.query('SELECT * FROM docente')

        res.json(allUsuario.rows)

    } catch (error) {
        next(error);
    }

}

module.exports = {
    login,
    getAllUsuario
}