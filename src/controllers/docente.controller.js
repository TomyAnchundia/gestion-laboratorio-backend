const pool = require("../db");

//lsitar docentes
const getAllDocentes = async (req, res, next) => {
  try {
    const allDocente = await pool.query("SELECT * FROM docente");
    res.json(allDocente.rows);
  } catch (error) {
    next(error);
  }
};

//usuario por id
const getDocente = async (req, res, next) => {
  try {
    const { id_docente } = req.params;
    const result = await pool.query(
      "SELECT * FROM docente WHERE id_docente = $1",
      [id_docente]
    );

    if (result.rows.length === 0)
      return res.status(404).json({
        message: "Docente no encontrado",
      });

    return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

//crear un docente (Registrarse)
const createDocente = async (req, res, next) => {
  try {
    const {
      nombre_docente,
      apellido_docente,
      telefono_docente,
      correo_docente,
      nombre_usuario,
      contrasena_usuario,
      usuario_admin,
    } = req.body;

    const result = await pool.query(
      "INSERT INTO docente (nombre_docente, apellido_docente, telefono_docente, correo_docente, nombre_usuario, contrasena_usuario, usuario_admin) VALUES ($1,$2,$3, $4, $5, $6, $7) RETURNING*",
      [
        nombre_docente,
        apellido_docente,
        telefono_docente,
        correo_docente,
        nombre_usuario,
        contrasena_usuario,
        usuario_admin,
      ]
    );

    res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

//eliminar un docente
const deleteDocente = async (req, res, next) => {
  try {
    const { id_docente } = req.params;
    const result = await pool.query(
      "DELETE FROM docente WHERE id_docente = $1 RETURNING *",
      [id_docente]
    );
    if (result.rowCount === 0)
      return res.status(404).json({
        message: "docente no encontrado",
      });
    return res.sendStatus(204);
  } catch (error) {
    next(error);
  }
};

//actualizar un docente
const updateDocente = async (req, res, next) => {
  try {
    const { id_docente } = req.params;
    const {
      nombre_docente,
      apellido_docente,
      telefono_docente,
      correo_docente,
      nombre_usuario,
      contrasena_usuario,
      
    } = req.body;
    const result = await pool.query(
      "UPDATE docente SET nombre_docente = $1, apellido_docente = $2, telefono_docente = $3, correo_docente = $4, nombre_usuario = $5, contrasena_usuario = $6 WHERE id_docente = $7 RETURNING* ",
      [
        nombre_docente,
        apellido_docente,
        telefono_docente,
        correo_docente,
        nombre_usuario,
        contrasena_usuario,
        id_docente,
      ]
    );
    if (result.rows.length === 0)
      return res.status(404).json({
        message: "docente no encontrado",
      });
    return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getAllDocentes,
  getDocente,
  createDocente,
  deleteDocente,
  updateDocente,
};
