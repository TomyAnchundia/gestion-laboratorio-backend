const pool = require('../db');


// listar usuarios
const getAllUsuario = async (req, res, next) => {
    try {
        const allUsuario = await pool.query('SELECT * FROM usuario')

        res.json(allUsuario.rows)

    } catch (error) {
        next(error);
    }

}

//obtener un usuario por id
//de aqui parte la logica para el login
const getUsuario = async (req, res, next) => {
    res.send('Retrieving a Usuario');
}

//crear un usuario
const createUsuario = async (req, res, next) => {
    res.send('Creating new usuario');
}

//eliminar un usuario
const deleteUsuario = async (req, res, next) => {
    res.send('Deleting usuario');
}

//actualizar un usuario
const updateUsuario = async (req, res, next) => {
    res.send('Updating usuario');
}

module.exports = {
    getAllUsuario,
    getUsuario,
    createUsuario,
    deleteUsuario,
    updateUsuario
}