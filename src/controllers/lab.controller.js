const pool = require("../db");

//lsitar laboratorios
const getAllLabs = async (req, res, next) => {
  try {
    const allLab = await pool.query("SELECT * FROM laboratorio");

    res.json(allLab.rows);
  } catch (error) {
    next(error);
  }
};

// obterner un laboratorio por id
const getLab = async (req, res, next) => {
  try {
    const { id_laboratorio } = req.params;
    const result = await pool.query(
      "SELECT * FROM laboratorio WHERE id_laboratorio = $1",
      [id_laboratorio]
    );

    if (result.rows.length === 0)
      return res.status(404).json({
        message: "Laboratorio no encontrada",
      });

    return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

//crear un nuevo laboratorio
const createLab = async (req, res, next) => {
  try {
    const { nombre_laboratorio, numero_maquinas, estado_laborartorio } =
      req.body;
    const result = await pool.query(
      "INSERT INTO laboratorio (nombre_laboratorio, numero_maquinas, estado_laborartorio ) VALUES ($1, $2, $3) RETURNING*",
      [nombre_laboratorio, numero_maquinas, estado_laborartorio,]
    );

    res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

//eliminar un laboratorio por id
const deleteLab = async (req, res, next) => {
  const { id_laboratorio } = req.params;
  try {
    const result = await pool.query(
      "DELETE FROM laboratorio WHERE id_laboratorio = $1 RETURNING*",
      [id_laboratorio]
    );

    if (result.rowCount === 0)
      return res.status(404).json({
        message: "laboratorio no encontrada",
      });
    return res.sendStatus(204);
  } catch (error) {
    next(error);
  }
};

//actualizar un laboratorio
const updateLab = async (req, res, next) => {
  try {
    const { id_laboratorio } = req.params;
    const { nombre_laboratorio, numero_maquinas, estado_laborartorio } =
      req.body;

    const result = await pool.query(
      "UPDATE laboratorio SET nombre_laboratorio = $1, numero_maquinas = $2, estado_laborartorio = $3 WHERE id_laboratorio = $4 RETURNING* ",
      [nombre_laboratorio, numero_maquinas, estado_laborartorio, id_laboratorio]
    );
    if (result.rows.length === 0)
      return res.status(404).json({
        message: "materia no encontrada",
      });
    return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getAllLabs,
  getLab,
  createLab,
  deleteLab,
  updateLab,
};
