const { Router } = require('express');
const {
    getAllDocentes,
    getDocente,
    createDocente,
    deleteDocente,
    updateDocente
} = require('../controllers/docente.controller')


const router = Router();


router.get('/docente', getAllDocentes)
router.get('/docente/:id_docente', getDocente)
router.post('/docente', createDocente)
router.delete('/docente/:id_docente', deleteDocente)
router.put('/docente/:id_docente', updateDocente)

module.exports = router;