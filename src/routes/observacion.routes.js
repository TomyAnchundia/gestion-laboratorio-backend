const { Router } = require('express');
const { getAllObservacion, getObservacion, createObservacion, deleteObservacion, updateObservacion } = require('../controllers/observacion.controller')

const router = Router();

router.get('/observacion', getAllObservacion)

router.get('/observacion/:id_observacion', getObservacion)

router.post('/observacion', createObservacion)

router.delete('/observacion/:id_observacion', deleteObservacion)

router.put('/observacion/:id_observacion', updateObservacion)


module.exports = router;